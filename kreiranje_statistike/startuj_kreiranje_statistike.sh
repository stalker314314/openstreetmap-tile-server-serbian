set -x

ZOOM=12
PUTANJA_DO_FAJLA=/var/log/apache2/access.log
#PUTANJA_DO_FAJLA=access.log


END=18
rm /var/www/html/statistika/data.js

for ((i=7;i<=END;i++)); do
    echo $i
    ZOOM=$i
    for mapa in cir lat; do
       cat $PUTANJA_DO_FAJLA | grep "$mapa/$ZOOM/" | awk -F\" '{print $2}' | awk '{print $2}' | sed '/^$/d' | sed 's/\?.*//g' | sort | uniq -c | sort -rn | python3 /home/renderer/kreiranje_statistike/pripremi.py $ZOOM $mapa >> /var/www/html/statistika/data.js
    done
done
