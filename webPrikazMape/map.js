
    

    var OSMSrbijaTileServerCir = L.tileLayer('/cir/{z}/{x}/{y}.png',{id: 'OSMSrbijaTileServerCir', maxZoom:18, attribution: '<a href="https://www.openstreetmap.rs" target="_blank">OpenStreeetMap Srbija</a>'});
    var OSMSrbijaTileServerLat = L.tileLayer('/lat/{z}/{x}/{y}.png',{id: 'OSMSrbijaTileServerLat', maxZoom:18, attribution: '<a href="https://www.openstreetmap.rs" target="_blank">OpenStreeetMap Srbija</a>'});
    var OSMSrbijaTiles = L.tileLayer.OSMSrbija();
    
    
    var map = L.map('map', {
      center: [44.2, 21],
      zoom: 8,
      minZoom: 7,
      layers: [OSMSrbijaTileServerCir]
    });
    
    var baseMaps = {
      "ОСМ Србија Ћирилица": OSMSrbijaTileServerCir,
      "OSM Srbija Latinica": OSMSrbijaTileServerLat,
      "Уклопљена мапа са глобалним OSM": OSMSrbijaTiles
    };


    L.control.layers(baseMaps, null, {collapsed: false}).addTo(map);

    L.control.scale().addTo(map);

    function onLocationFound(e) {
      var radius = e.accuracy / 2;

      L.marker(e.latlng).addTo(map)
        .bindPopup("You are here. Accuracy in meters: " + radius).openPopup();

      L.circle(e.latlng, radius).addTo(map);
    }
    
    var hash = new L.Hash(map);
    
    map.on('locationfound', onLocationFound);
    
    
    
    var mapControlsContainer = document.getElementsByClassName("leaflet-control")[0];
    var logoContainer = document.getElementById("logoContainer");

    mapControlsContainer.appendChild(logoContainer);    
    
/*
    function onLocationError(e) {
      alert(e.message);
    }
    map.on('locationerror', onLocationError);    

    map.locate({setView: true, maxZoom: 16});
*/    
    //L.tileLayer('http://osmid.uzice.net/maps/tile.php?z={z}&x={x}&y={y}',{maxZoom:17}).addTo(map);
