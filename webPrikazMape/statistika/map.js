
    

    var OSMSrbijaTileServerCir = L.tileLayer('/cir/{z}/{x}/{y}.png',{id: 'OSMSrbijaTileServerCir', maxZoom:18, attribution: '<a href="https://www.openstreetmap.rs" target="_blank">OpenStreeetMap Srbija</a>'});
    var OSMSrbijaTileServerLat = L.tileLayer('/lat/{z}/{x}/{y}.png',{id: 'OSMSrbijaTileServerLat', maxZoom:18, attribution: '<a href="https://www.openstreetmap.rs" target="_blank">OpenStreeetMap Srbija</a>'});
    var OSMSrbijaTiles = L.tileLayer.OSMSrbija();
    
    
    var map = L.map('map', {
      center: [44.2, 21],
      zoom: 8,
      minZoom: 7,
      layers: [OSMSrbijaTileServerCir]
    });
    
    var baseMaps = {
      "ОСМ Србија Ћирилица": OSMSrbijaTileServerCir,
      "OSM Srbija Latinica": OSMSrbijaTileServerLat,
      "Уклопљена мапа са глобалним OSM": OSMSrbijaTiles
    };

    var heat_cir_7 = L.heatLayer(podaci_cir_7,{radius: 20, blur: 15});
    var heat_cir_8 = L.heatLayer(podaci_cir_8,{radius: 20, blur: 15});
    var heat_cir_9 = L.heatLayer(podaci_cir_9,{radius: 20, blur: 15});
    var heat_cir_10 = L.heatLayer(podaci_cir_10,{radius: 20, blur: 15});
    var heat_cir_11 = L.heatLayer(podaci_cir_11,{radius: 20, blur: 15});
    var heat_cir_12 = L.heatLayer(podaci_cir_12,{radius: 20, blur: 15});
    var heat_cir_13 = L.heatLayer(podaci_cir_13,{radius: 20, blur: 15});
    var heat_cir_14 = L.heatLayer(podaci_cir_14,{radius: 20, blur: 15});
    var heat_cir_15 = L.heatLayer(podaci_cir_15,{radius: 20, blur: 15});
    var heat_cir_16 = L.heatLayer(podaci_cir_16,{radius: 20, blur: 15});
    var heat_cir_17 = L.heatLayer(podaci_cir_17,{radius: 20, blur: 15});
    var heat_cir_18 = L.heatLayer(podaci_cir_18,{radius: 20, blur: 15});

    var heat_lat_7 = L.heatLayer(podaci_lat_7,{radius: 20, blur: 15});
    var heat_lat_8 = L.heatLayer(podaci_lat_8,{radius: 20, blur: 15});
    var heat_lat_9 = L.heatLayer(podaci_lat_9,{radius: 20, blur: 15});
    var heat_lat_10 = L.heatLayer(podaci_lat_10,{radius: 20, blur: 15});
    var heat_lat_11 = L.heatLayer(podaci_lat_11,{radius: 20, blur: 15});
    var heat_lat_12 = L.heatLayer(podaci_lat_12,{radius: 20, blur: 15});
    var heat_lat_13 = L.heatLayer(podaci_lat_13,{radius: 20, blur: 15});
    var heat_lat_14 = L.heatLayer(podaci_lat_14,{radius: 20, blur: 15});
    var heat_lat_15 = L.heatLayer(podaci_lat_15,{radius: 20, blur: 15});
    var heat_lat_16 = L.heatLayer(podaci_lat_16,{radius: 20, blur: 15});
    var heat_lat_17 = L.heatLayer(podaci_lat_17,{radius: 20, blur: 15});
    var heat_lat_18 = L.heatLayer(podaci_lat_18,{radius: 20, blur: 15});
    
    
    var overlayMaps = {
      "Statistika upotrebe nivoa 7 cir": heat_cir_7,
      "Statistika upotrebe nivoa 8 cir": heat_cir_8,
      "Statistika upotrebe nivoa 9 cir": heat_cir_9,
      "Statistika upotrebe nivoa 10 cir": heat_cir_10,
      "Statistika upotrebe nivoa 11 cir": heat_cir_11,
      "Statistika upotrebe nivoa 12 cir": heat_cir_12,
      "Statistika upotrebe nivoa 13 cir": heat_cir_13,
      "Statistika upotrebe nivoa 14 cir": heat_cir_14,
      "Statistika upotrebe nivoa 15 cir": heat_cir_15,
      "Statistika upotrebe nivoa 16 cir": heat_cir_16,
      "Statistika upotrebe nivoa 17 cir": heat_cir_17,
      "Statistika upotrebe nivoa 18 cir": heat_cir_18,
      "Statistika upotrebe nivoa 7 lat": heat_lat_7,
      "Statistika upotrebe nivoa 8 lat": heat_lat_8,
      "Statistika upotrebe nivoa 9 lat": heat_lat_9,
      "Statistika upotrebe nivoa 10 lat": heat_lat_10,
      "Statistika upotrebe nivoa 11 lat": heat_lat_11,
      "Statistika upotrebe nivoa 12 lat": heat_lat_12,
      "Statistika upotrebe nivoa 13 lat": heat_lat_13,
      "Statistika upotrebe nivoa 14 lat": heat_lat_14,
      "Statistika upotrebe nivoa 15 lat": heat_lat_15,
      "Statistika upotrebe nivoa 16 lat": heat_lat_16,
      "Statistika upotrebe nivoa 17 lat": heat_lat_17,
      "Statistika upotrebe nivoa 18 lat": heat_lat_18
    };    
    
    
    
    L.control.layers(baseMaps, overlayMaps, {collapsed: false}).addTo(map);
    
    
    L.control.scale().addTo(map);

    function onLocationFound(e) {
      var radius = e.accuracy / 2;

      L.marker(e.latlng).addTo(map)
        .bindPopup("You are here. Accuracy in meters: " + radius).openPopup();

      L.circle(e.latlng, radius).addTo(map);
    }
    
    var hash = new L.Hash(map);
    
    map.on('locationfound', onLocationFound);
    
    
    
    var mapControlsContainer = document.getElementsByClassName("leaflet-control")[0];
    var logoContainer = document.getElementById("logoContainer");

    mapControlsContainer.appendChild(logoContainer);    
    
/*
    function onLocationError(e) {
      alert(e.message);
    }
    map.on('locationerror', onLocationError);    

    map.locate({setView: true, maxZoom: 16});
*/    
    //L.tileLayer('http://osmid.uzice.net/maps/tile.php?z={z}&x={x}&y={y}',{maxZoom:17}).addTo(map);
